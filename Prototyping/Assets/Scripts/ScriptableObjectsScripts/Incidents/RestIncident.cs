using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;


[CreateAssetMenu(menuName = "ScriptableObjects/RestIncident")]
public class RestIncident : ExplorationIncident
{
    // public int healAmount;
    // public int maxHpIncreaseAmount;
    public List<string> consequenceTexts = new List<string>();
    public GameEvent playerHpChange;
    public GameEvent playerMaxHpChange;
    public override void AddListenersToButtons()
    {
        for(int i = 0; i < numberOfChoices; i ++){
            Button button = incidentCanvas.GetComponent<IncidentCanvasElements>().choiceButtonList[i];
            if(i == 0){

                button.onClick.AddListener(delegate{playerHpChange.Raise(choiceValueList[0]);});
            }
            else if(i == 1){
                button.onClick.AddListener(delegate{playerMaxHpChange.Raise(choiceValueList[1]);});
            }
            button.onClick.AddListener(delegate{ChangeConsequenceText(button.transform.GetSiblingIndex());});



        }
    }
    private void ChangeConsequenceText(int index){
        Debug.Log("Index Value is: " + index);
        incidentCanvas.GetComponent<IncidentCanvasElements>().consequenceText.text = consequenceTexts[index];
    }


}
