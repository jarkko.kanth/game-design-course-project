using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using TMPro;

//[CreateAssetMenu(menuName = "ScriptableObjects/ExplorationIncident")]

public abstract class ExplorationIncident : ScriptableObject
{
    public string incidentName;
    public Canvas incidentCanvasPrefab;
    protected Canvas incidentCanvas;

    public int numberOfChoices;
    public string descriptionText;
    public Sprite background;
    public Sprite incidentImage;
    public List<string> choiceTextList = new List<string>();
    public string proceedText;
    public List<int> choiceValueList = new List<int>();


    public void CreateIncident(){
        ApplyUiVisualsToCanvas();
        AddListenersToButtons();
    }


    public void ApplyUiVisualsToCanvas(){
        incidentCanvas = GameObject.Instantiate(incidentCanvasPrefab);
       // incidentCanvas.transform.SetAsLastSibling();
        IncidentCanvasElements canvasElements = incidentCanvas.GetComponent<IncidentCanvasElements>();
        canvasElements.descriptionText.text = descriptionText;
        canvasElements.proceedButton.GetComponentInChildren<TMP_Text>().text = proceedText;
        for(int i = 0; i < numberOfChoices; i ++){
            canvasElements.choiceButtonList[i].gameObject.SetActive(true);
            canvasElements.choiceButtonList[i].GetComponentInChildren<TMP_Text>().text = choiceTextList[i];  
        }
        canvasElements.incidentBackground.sprite = background;
        canvasElements.incidentImage.sprite = incidentImage;
        
    }
    public abstract void AddListenersToButtons();



}
