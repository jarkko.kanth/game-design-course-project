using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObjects/CombatIncident")]
public class CombatIncident : ExplorationIncident
{
    public Enemy enemy;
    public EnemyList enemyList;
    public GameEvent combatStart;
    public Sprite[] enemySplashArts;
    public FloatVariable enemiesDefeated;
    public override void AddListenersToButtons()
    {
        for(int i = 0; i < numberOfChoices; i ++){
            if(i == 0){
                // enemyList.list.Clear();
                // enemyList.list.Add(enemy);
                //enemy.isDead = false;
                incidentCanvas.GetComponent<IncidentCanvasElements>().choiceButtonList[i].onClick.AddListener(delegate{combatStart.Raise();});
                //incidentCanvas.gameObject.SetActive(false);
                incidentCanvas.GetComponent<IncidentCanvasElements>().choiceButtonList[i].onClick.AddListener(DisableCanvas);
            }
            else if(i == 1){
            }

        }
        incidentCanvas.GetComponent<IncidentCanvasElements>().incidentBackground.sprite = enemySplashArts[(int)enemiesDefeated.value];
    }
    void DisableCanvas(){
        incidentCanvas.gameObject.SetActive(false);
    }
}
