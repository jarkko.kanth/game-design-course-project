using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;


[CreateAssetMenu(menuName = "ScriptableObjects/Enemy")]
public class Enemy : EntityInformationHolder
{
    public int enemyId;
    public int numberOfActions;
    public Sprite enemySprite;
    public Sprite background;
    public AudioClip enemyBgm;



    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
