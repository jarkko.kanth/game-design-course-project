using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntityInformationHolder : ScriptableObject
{
    public string entityName;

    public FloatVariableWithMax hp;
    public FloatVariableWithMax shield;
    public FloatVariableWithMax shieldLeft;
    public FloatVariableWithMax shieldRight;
    public ActionListScriptableObject listOfActions;
    public bool isDead;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
