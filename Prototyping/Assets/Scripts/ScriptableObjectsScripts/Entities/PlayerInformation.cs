using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObjects/PlayerInformation")]

public class PlayerInformation : EntityInformationHolder
{
    public FloatVariableWithMax stamina;
    public FloatVariableWithMax fuel;
    public bool parryIsActive;
    public bool sidestepIsActive;
    public ActionListScriptableObject currentMoveset;
    public MovesetList availableMovesets;
    public int currentMovesetIndex = 0;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
