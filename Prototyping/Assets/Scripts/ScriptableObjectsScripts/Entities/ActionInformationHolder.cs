using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


[CreateAssetMenu(menuName = "ScriptableObjects/ActionInformationHolder")]
public class ActionInformationHolder : ScriptableObject
{
    public string actionName;
    public string actionDescription;
    public string actionType;
    public string damageSubtype;
    public bool isPlayerAction;
    public EntityInformationHolder user;
    public int id;
    public float baseStaminaCost;
    public float staminaModifier;
    public float baseValue;
    public float valueModifier;
    public FloatVariable chiTypeUsed;
    public bool usesFuel;
    public bool isTargetingLeft = true;
    //public bool isActive;
    public bool targetsSelf;
    public GameObject actionIcon;
    public GameObject actionButton;
    public Sprite actionIconSprite;
    public AnimationClip animation;
    

    //public void ActivateAction(){}

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
