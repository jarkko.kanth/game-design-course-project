using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName = "ScriptableObjects/ActionsWaitingToExecuteList")]

public class ActionsWaitingToExecuteList : ScriptableObject
{
    public List<ActionInformationHolder> playerHead;
    public List<ActionInformationHolder> playerLeft;
    public List<ActionInformationHolder> playerRight;

    
    public List<ActionInformationHolder> enemyHead;
    public List<ActionInformationHolder> enemyLeft;
    public List<ActionInformationHolder> enemyRight;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
