using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

[CreateAssetMenu(menuName = "Events/GameEvent")]
public class GameEvent : ScriptableObject
{
    protected List<GameEventListener> listeners = new List<GameEventListener>();
    public void Raise(){
        for(int i = listeners.Count -1; i >= 0; i--){
            listeners[i].OnEventRaised();
        }
    }
    public  void Raise(int value){
        //Debug.Log("Event raised, Parameter: " + value);
        for(int i = listeners.Count -1; i >= 0; i--){
            listeners[i].OnEventRaised(value);
        }
    }
    // public void Raise(Enemy enemy){
    //     for(int i = listeners.Count -1; i >= 0; i--){
    //         listeners[i].OnEventRaised(enemy);
    //     }
    // }

    public void RegisterListener(GameEventListener listener){
        if (!listeners.Contains(listener))
            listeners.Add(listener);
    }
    public void UnRegisterListener(GameEventListener listener){
        if (listeners.Contains(listener))
            listeners.Remove(listener);
    }
}
