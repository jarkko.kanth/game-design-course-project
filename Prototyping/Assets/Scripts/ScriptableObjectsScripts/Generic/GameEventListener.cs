using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GameEventListener : MonoBehaviour
{
    public GameEvent Event;
    public UnityEvent Response;
    public UnityEvent<int> ResponseWithNumber;
    //public UnityEvent<Enemy> ResponseWithEnemy;

    private void OnEnable(){
        Event.RegisterListener(this);
    }
        private void OnDisable(){
        Event.UnRegisterListener(this);
    }
    public void OnEventRaised(){
        //Debug.Log("Invoking event without parameter");
        Response.Invoke();
    }
    public void OnEventRaised(int index){
        //Debug.Log("Invoking event with parameter: " + index);
        ResponseWithNumber.Invoke(index);
    }
    // public void OnEventRaised(Enemy enemy){
    //     ResponseWithEnemy.Invoke(enemy);
    // }


}
