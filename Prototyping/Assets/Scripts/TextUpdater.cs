using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


public class TextUpdater : MonoBehaviour
{
    public FloatVariableWithMax variable;
    public TMP_Text text;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    public void UpdateText()
    {
        text.text = variable.value.ToString() + "/" + variable.max.ToString();

    }
}
