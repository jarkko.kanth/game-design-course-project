using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : MonoBehaviour
{
    public EnemyList listOfEnemies;
    // Start is called before the first frame update
    void Start()
    {
        foreach(Enemy enemy in listOfEnemies.list){
            enemy.hp.value = enemy.hp.max;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
