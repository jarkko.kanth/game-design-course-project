using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Events;
using TMPro;
using UnityEngine.UI;
using UnityEngine.Audio;

public class FightManager : MonoBehaviour
{
    //public ActionTrackManager actionTrackManager;
    public PlayerActionUiManager playerActionUiManager;
    public ActionsWaitingToExecuteList actionsWaitingToExecuteList;
    public EnemyList enemyList;
    public PlayerInformation playerInformation;
    //public Enemy giantEnemyCrab;
    public FloatVariableWithMax playerHp;
    public FloatVariableWithMax playerStamina;
    public FloatVariableWithMax staminaRecovery;
    public FloatVariable enemiesDefeated;
    public FloatVariable encountersCleared;
    public Canvas fightCanvas;
    public Canvas victoryCanvas;
    public Canvas defeatCanvas;
    public TMP_Text usedActionText;
    public GameObject endTurnButton;
    public GameObject enemyStatsParent;
    public UnityEvent refreshUi;
    public Animator playerEffectAnimator;
    public Animator enemyEffectAnimator;
    public SpriteRenderer enemyImage;
    public SpriteRenderer background;
    public AudioSource bgm;

    //private int targetIndex = 0;
    //private bool allEnemiesDead = false;
    private bool clearPlayerAttacks = false;
    //private bool isAnimPlaying = false;
    private List<List<ActionInformationHolder>> PlayerActionQueue =  new List<List<ActionInformationHolder>>();
    private List<List<ActionInformationHolder>> EnemyActionQueue = new List<List<ActionInformationHolder>>();
    private Enemy currentEnemy;

    public bool debugMode = false;
    public bool easyMode = false;
    // Start is called before the first frame update
    void Start()
    {
        if(debugMode){
            DebugMode();
        }
        playerInformation.currentMoveset = playerInformation.availableMovesets.list[playerInformation.currentMovesetIndex];
        currentEnemy = enemyList.list[(int)enemiesDefeated.value];

        
        PlayerActionQueue.Add(actionsWaitingToExecuteList.playerHead);
        PlayerActionQueue.Add(actionsWaitingToExecuteList.playerLeft);
        PlayerActionQueue.Add(actionsWaitingToExecuteList.playerRight);
        EnemyActionQueue.Add(actionsWaitingToExecuteList.enemyHead);
        EnemyActionQueue.Add(actionsWaitingToExecuteList.enemyLeft);
        EnemyActionQueue.Add(actionsWaitingToExecuteList.enemyRight);



        foreach(Enemy enemy in enemyList.list){
            enemy.hp.value = enemy.hp.max;
            enemy.shield.value = 0;
            enemy.isDead = false;
            if(easyMode){
                enemy.hp.value = 1;
            }
        }
        bgm.clip = currentEnemy.enemyBgm;
        bgm.Play();
        enemyImage.sprite = currentEnemy.enemySprite;
        background.sprite = currentEnemy.background;
        enemyStatsParent.GetComponentsInChildren<ImageFillSetter>()[0].variable = currentEnemy.hp;
        enemyStatsParent.GetComponentsInChildren<ImageFillSetter>()[1].variable = currentEnemy.shield;
        enemyStatsParent.GetComponentsInChildren<TextUpdater>()[0].variable = currentEnemy.hp;
        enemyStatsParent.GetComponentsInChildren<TextUpdater>()[1].variable = currentEnemy.shield;

        AssignNewEnemyAttacks();


        // foreach(Enemy enemy in enemyList.list){
        //     actionsWaitingToExecuteList.listTurn1.Add(enemy.listOfActions.list[Random.Range(0, enemy.listOfActions.list.Count)]);
        // }
        // foreach(Enemy enemy in enemyList.list){
        //     actionsWaitingToExecuteList.listTurn2.Add(enemy.listOfActions.list[Random.Range(0, enemy.listOfActions.list.Count)]);
        // }
        // foreach(Enemy enemy in enemyList.list){
        //     actionsWaitingToExecuteList.listTurn3.Add(enemy.listOfActions.list[Random.Range(0, enemy.listOfActions.list.Count)]);
        // }
        //actionTrackManager.UpdateActionTrack();

        refreshUi.Invoke();

    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void OnEndTurnClick(){
        endTurnButton.SetActive(false);

        StartCoroutine(ExecuteActions());

        // ExecuteEnemyActions();
        // TurnEndCleanup();
    }

    IEnumerator ExecuteActions(){
        //Loop through the Head/Left/Right action lists and perform actions in them if there are any
        for(int i = 0; i < PlayerActionQueue.Count && i < EnemyActionQueue.Count; i++){
            //Loops player actions and executes them. Looped in reverse order so that we can remove the action from the queue so that we can update the UI
            for(int j = PlayerActionQueue[i].Count -1; j >= 0; j--){
                ActionInformationHolder action = PlayerActionQueue[i][j];
                switch(action.id){
                    case 0:
                        Debug.Log(action.actionName+" was used");
                        DealDamage(action.baseValue, currentEnemy, i);
                        break;
                    case 1:
                        Debug.Log(action.actionName + "was used");
                        DealDamage(action.baseValue, currentEnemy, i);
                        break;
                    case 2:
                        Debug.Log(action.actionName + "was used");
                        playerInformation.parryIsActive = true;
                        // if (EnemyActionQueue[i].Count > 0){
                        //     EnemyActionQueue[i].RemoveAt(EnemyActionQueue.Count-1);
                        // }
                        // if(i == 1){
                        //     playerInformation.shieldLeft.value =+ action.baseValue;
                        // }
                        // else if(i == 2){
                        //     playerInformation.shieldRight.value =+ action.baseValue;
                        // }
                        
                        break;
                    case 3:
                        Debug.Log(action.actionName + "was used");
                        DealDamage(action.baseValue, currentEnemy, i);
                        break;
                    case 4:
                        Debug.Log(action.actionName + "was used");
                        currentEnemy.shield.value = 0;
                        break;
                    case 5:
                        Debug.Log(action.actionName + "was used");
                        playerInformation.sidestepIsActive = true;
                        if(i == 1){
                            playerInformation.shieldLeft.value =+ action.baseValue;
                        }
                        else if(i == 2){
                            playerInformation.shieldRight.value =+ action.baseValue;
                        }
                        break;
                    case 6:
                        Debug.Log(action.actionName + "was used");
                        // if(playerInformation.currentMovesetIndex >= (playerInformation.availableMovesets.list.Count -1)){
                        //     playerInformation.currentMovesetIndex = 0;
                        // }
                        // else{
                        //     playerInformation.currentMovesetIndex++;
                        // }
                        // playerInformation.currentMoveset = playerInformation.availableMovesets.list[playerInformation.currentMovesetIndex];
                        // clearPlayerAttacks = true;
                        // playerActionUiManager.UpdatePlayerUi();
                        break;
                    case 7:
                        Debug.Log(action.actionName + "was used");
                        playerInformation.fuel.value += action.baseValue;
                        if(playerInformation.fuel.value > playerInformation.fuel.max){
                            playerInformation.fuel.value = playerInformation.fuel.max;
                        }
                        refreshUi.Invoke();
                        break;

                }
                usedActionText.text = "You used " + action.actionName + "!";
                //Plays the animation assossiated with the action. Currently only waits for 1sec for the animation to complete
                //Should add a check to make it wait until animation has been completed.
                if(action.targetsSelf){
                    playerEffectAnimator.Play(action.animation.name);        

                }
                else{
                    enemyEffectAnimator.Play(action.animation.name);
                }
                yield return new WaitForSeconds(1);
                PlayerActionQueue[i].Remove(action);
                refreshUi.Invoke();
            }
            //Checks if all enemies are dead before performing their actions.
            
            if(currentEnemy.isDead){
                CombatVictory();
                StopCoroutine(ExecuteActions());
                break;
            }

            //Loops through enemy actions and performs them.
            for(int j = EnemyActionQueue[i].Count -1; j >= 0; j--){
                ActionInformationHolder action = EnemyActionQueue[i][j];
                switch(action.id){
                    case 0:
                        Debug.Log(action.actionName + "was used");
                        DealDamage(action.baseValue, playerInformation, i);
                        break;
                    case 1:
                        Debug.Log(action.actionName + "was used");
                        DealDamage(action.baseValue, playerInformation, i);
                        break;
                    case 2:
                        Debug.Log(action.actionName + "was used");
                        currentEnemy.shield.value += action.baseValue;
                        if(currentEnemy.shield.value > currentEnemy.shield.max){
                            currentEnemy.shield.value = currentEnemy.shield.max;
                        }
                        refreshUi.Invoke();
                        break;
                    case 3:
                        Debug.Log(action.actionName + "was used");
                        staminaRecovery.value -= action.baseValue;
                        break;
                    case 4:
                        Debug.Log(action.actionName + "was used");
                        playerInformation.hp.max -= 5;
                        break;
                    case 5:
                        Debug.Log(action.actionName + "was used");
                        break;

                }
                usedActionText.text = currentEnemy.entityName + " used " + action.actionName + "!";
                if(action.targetsSelf){
                    enemyEffectAnimator.Play(action.animation.name);        
                }
                else{
                    playerEffectAnimator.Play(action.animation.name);
                }
                yield return new WaitForSeconds(1);
                EnemyActionQueue[i].Remove(action);
                refreshUi.Invoke();
            }
            if(playerInformation.isDead){
                CombatDefeat();
                StopCoroutine(ExecuteActions());
                break;
            }
        }

        if(clearPlayerAttacks){
            ClearAttacks(PlayerActionQueue);
            clearPlayerAttacks = false;
        }
        TurnEndCleanup();
        //StartCoroutine(ExecuteEnemyActions());

    }
    // public IEnumerator ExecuteEnemyActions(){
    //     //foreach(List<ActionInformationHolder> list in EnemyActionQueue){
    //     for(int i = 0; i < EnemyActionQueue.Count; i++){
    //         //foreach(ActionInformationHolder action in EnemyActionQueue[i]){
    //         for(int j = EnemyActionQueue[i].Count -1; j >= 0; j--){
    //             ActionInformationHolder action = EnemyActionQueue[i][j];
    //             switch(action.id){
    //                 case 0:
    //                     Debug.Log("Pinch was used");
    //                     DealDamage(action.baseValue, playerInformation, i);
    //                     break;
    //                 case 1:
    //                     Debug.Log("Claw was used");
    //                     DealDamage(action.baseValue, playerInformation, i);
    //                     break;
    //                 case 2:
    //                     Debug.Log("Defend was used");
    //                     action.user.shield.value += action.baseValue;
    //                     if(action.user.shield.value > action.user.shield.max){
    //                         action.user.shield.value = action.user.shield.max;
    //                     }
    //                     refreshUi.Invoke();
    //                     break;
    //                 case 3:
    //                     Debug.Log("Debuff was used");
    //                     staminaRecovery.value -= action.baseValue;
    //                     break;
    //             }
    //             Debug.Log("Start playing animation " + action.animation.name);
    //             testAnimator.Play(action.animation.name);        
    //             yield return new WaitForSeconds(1);
    //             Debug.Log("After animation was played");
    //             EnemyActionQueue[i].Remove(action);
    //             refreshUi.Invoke();
    //         }
    //     }
    //     ClearAttacks(EnemyActionQueue);
    //     if(playerInformation.isDead){
    //         CombatDefeat();
    //     }
    //     yield return new WaitForSeconds(1);
    //     TurnEndCleanup();
    // }

    public void DealDamage(float damage, EntityInformationHolder target, int side){
        FloatVariableWithMax targetedShield;
        if(target.GetType().Equals(typeof(PlayerInformation))){
            //Debug.Log("Targeting player");
            if(side == 1){
                targetedShield = playerInformation.shieldLeft;
                //Debug.Log("Targeting left Shield of player");
            }
            else if(side == 2){
                targetedShield = playerInformation.shieldRight;
                //Debug.Log("Targeting right Shield of player");
            }
            else{
                targetedShield = playerInformation.shieldLeft;
                Debug.Log("This shouldn't happen");
            }
            //targetedShield = target.shield.value;
        }
        else{
            targetedShield = target.shield;
        }
        targetedShield.value -= damage;
        if(targetedShield.value < 0){
            target.hp.value += targetedShield.value;
            targetedShield.value = 0;
        }
        if(target.hp.value <= 0){
            target.isDead = true;
        }
        refreshUi.Invoke();    
    }
    public void ClearAttacks(List<List<ActionInformationHolder>> queueToClear){
        foreach(List<ActionInformationHolder> list in queueToClear){
            list.Clear();     
        }
    }

    public void TurnEndCleanup(){
        // actionsWaitingToExecuteList.listTurn1.Clear();
        // actionsWaitingToExecuteList.listTurn1.AddRange(actionsWaitingToExecuteList.listTurn2);
        // actionsWaitingToExecuteList.listTurn2.Clear();
        // actionsWaitingToExecuteList.listTurn2.AddRange(actionsWaitingToExecuteList.listTurn3);
        // actionsWaitingToExecuteList.listTurn3.Clear();
        usedActionText.text = "";
        playerStamina.value = staminaRecovery.value;
        staminaRecovery.value = staminaRecovery.max;
        if(playerStamina.value > playerStamina.max){
            playerStamina.value = playerStamina.max;
        }
        refreshUi.Invoke();
        playerInformation.shieldLeft.value = 0;
        playerInformation.shieldRight.value = 0;
        ClearAttacks(PlayerActionQueue);
        AssignNewEnemyAttacks();
        endTurnButton.SetActive(true);

        refreshUi.Invoke();
        //actionTrackManager.UpdateActionTrack();
    }
    public void AssignNewEnemyAttacks(){
        for(int i = 0; i < currentEnemy.numberOfActions; i++){
            ActionInformationHolder actionToBeAssigned = currentEnemy.listOfActions.list[Random.Range(0, currentEnemy.listOfActions.list.Count)];
            if ( actionToBeAssigned.actionType == "Buff" || actionToBeAssigned.actionType == "Debuff"){
                EnemyActionQueue[0].Add(actionToBeAssigned);
            }
            else{
                EnemyActionQueue[Random.Range(1, EnemyActionQueue.Count)].Add(actionToBeAssigned);
            }
        }   
    }

    void OnDestroy(){
        //playerHp.value = playerHp.max;
        playerStamina.value = playerStamina.max;
        //enemyList.list.Clear();
    }

    void CombatVictory(){
        Debug.Log("You won!");
        EndOfCombatCleanup();
        enemiesDefeated.value++;
        encountersCleared.value++;
        refreshUi.Invoke();
        //actionTrackManager.UpdateActionTrack();
        fightCanvas.gameObject.SetActive(false);
        victoryCanvas.gameObject.SetActive(true);

    }
    void CombatDefeat(){
        Debug.Log("You Lost!");
        EndOfCombatCleanup();
        refreshUi.Invoke();
        //actionTrackManager.UpdateActionTrack();
        fightCanvas.gameObject.SetActive(false);
        defeatCanvas.gameObject.SetActive(true);
    }

    void EndOfCombatCleanup(){
        ClearAttacks(PlayerActionQueue);
        ClearAttacks(EnemyActionQueue);
        playerInformation.fuel.value = playerInformation.fuel.max;
        staminaRecovery.value = staminaRecovery.max;
        refreshUi.Invoke();

    }

    public void BackToMainMenu(){
        SceneManager.LoadScene(0);
    }
    public void ContinueAfterVictory(){
        if(enemiesDefeated.value > 2){
             SceneManager.LoadScene(3);
        }
        else{
             SceneManager.LoadScene(1);
        }

    }


    public void DebugMode(){
        //enemyList.list.Add(giantEnemyCrab);
        enemiesDefeated.value = 0;
        playerInformation.fuel.value = playerInformation.fuel.max;
        staminaRecovery.value = staminaRecovery.max;
        playerHp.value = playerHp.max;
    }
    IEnumerator PlayAnimation(){
        //isAnimPlaying = true;
        yield return new WaitForSeconds(5);
        Debug.Log("Animation ended");
       // isAnimPlaying = false;

    }
}
