using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlayerHpManager : MonoBehaviour
{
    public FloatVariableWithMax playerHp;
    public UnityEvent refreshUi;

    public void ChangePlayerHp(int amount){
        Debug.Log("Player HP changed!");
        playerHp.value += amount;
        if(playerHp.value > playerHp.max){
        playerHp.value = playerHp.max;
        }
        refreshUi.Invoke();
    }
    public void ChangePlayerMaxHp(int amount){
        Debug.Log("Player Max HP changed!");
        playerHp.max += amount;
        if(playerHp.value>playerHp.max){
            playerHp.value = playerHp.max;
        }
        refreshUi.Invoke();
    }
}
