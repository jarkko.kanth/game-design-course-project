using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Events;
using UnityEngine.UI;

public class ExplorationManager : MonoBehaviour
{
    // public FloatVariableWithMax playerHp;
    // public GameObject choiceMenuParent;
    // public GameObject choiceButtonPrefab;
    // public GameObject proceedMenuParent;
    public UnityEvent refreshUi;
    public UnityEvent explorationStart;
    public FloatVariable completedEncounters;
    public GameObject explorationCanvas;
    
    // Start is called before the first frame update
    void Start()
    {
        // if(completedEncounters.value > 2){
        //     SceneManager.LoadScene(3);
        // }
        // else    {
        //     refreshUi.Invoke();
        //     explorationStart.Invoke();
        // }
        refreshUi.Invoke();
        explorationStart.Invoke();


    }

    // Update is called once per frame

    public void StartCombat(){
        SceneManager.LoadScene(2);
    }

    public void ActivateExplorationIncident(ExplorationIncident incident){
        incident.CreateIncident();
        explorationCanvas.SetActive(false);
    }
    public void StartExploration(){
        
    }
}
