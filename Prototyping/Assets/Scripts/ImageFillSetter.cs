using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ImageFillSetter : MonoBehaviour
{
    public FloatVariableWithMax variable;
    public Image image;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    public void UpdateFillAmount()
    {
        image.fillAmount = Mathf.Clamp01(variable.value/variable.max);
    }
}
