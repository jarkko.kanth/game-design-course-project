using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ActionTrackManager : MonoBehaviour
{
    public ActionsWaitingToExecuteList actionList;
    public Transform headParent;
    public Transform leftParent;
    public Transform rightParent;
    public UnityEvent refreshUi;
    public FloatVariable index;
    private List<Transform> listOfParents = new List<Transform>();
    private List<List<ActionInformationHolder>> playerActionQueue = new List<List<ActionInformationHolder>>();
    private List<List<ActionInformationHolder>> enemyActionQueue = new List<List<ActionInformationHolder>>();


    // Start is called before the first frame update
    void Start()
    {
        listOfParents.Add(headParent);
        listOfParents.Add(leftParent);
        listOfParents.Add(rightParent);
        playerActionQueue.Add(actionList.playerHead);
        playerActionQueue.Add(actionList.playerLeft);
        playerActionQueue.Add(actionList.playerRight);
        enemyActionQueue.Add(actionList.enemyHead);
        enemyActionQueue.Add(actionList.enemyLeft);
        enemyActionQueue.Add(actionList.enemyRight);

        UpdateActionTrack();
    }

    // Update is called once per frame
    void Update()
    {

    }
    void OnDestroy(){
        foreach(List<ActionInformationHolder> list in playerActionQueue){
            list.Clear();
        }
        foreach(List<ActionInformationHolder> list in enemyActionQueue){
            list.Clear();
        }
        // actionList.playerHead.Clear();
        // actionList.playerLeft.Clear();
        // actionList.playerRight.Clear();
    }
    public void UpdateActionTrack(){
        ClearActionTrack();
        // foreach (Transform child in turn1Parent){
        //     Destroy(child.gameObject);
        // }
        for(int i = 0; i < playerActionQueue.Count; i++){
            foreach(ActionInformationHolder action in playerActionQueue[i]){
                GameObject icon = Instantiate(action.actionIcon, listOfParents[i]);
                icon.GetComponent<ActionIconTooltip>().tooltipText = action.actionDescription;
            }
        }
        for(int i = 0; i < enemyActionQueue.Count; i++){
            foreach(ActionInformationHolder action in enemyActionQueue[i]){
                GameObject icon = Instantiate(action.actionIcon, listOfParents[i]);
                icon.GetComponent<ActionIconTooltip>().tooltipText = action.actionDescription;
            }
        }


        
        // foreach(ActionInformationHolder action in actionList.listTurn2){
        //     GameObject.Instantiate(action.actionIcon, turn2Parent);
        // }
        // foreach(ActionInformationHolder action in actionList.listTurn3){
        //     GameObject.Instantiate(action.actionIcon, turn3Parent);
        // }
    }

    public void ClearActionTrack(){
        foreach(Transform parent in listOfParents){
            foreach (Transform child in parent){
                Destroy(child.gameObject);
            }
        }
    }

    public void  SetIndex(int value){
        index.value = value;
    }
}
