using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ActionIconTooltip : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public string tooltipText;
        public void OnPointerEnter(PointerEventData eventData){
        TooltipManager.ShowTooltip_Static(tooltipText);
    }
    public void OnPointerExit(PointerEventData eventData){
        TooltipManager.HideTooltip_Static();
    }

}
