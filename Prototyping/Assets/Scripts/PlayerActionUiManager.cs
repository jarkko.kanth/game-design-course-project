using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class PlayerActionUiManager : MonoBehaviour
{
    public GameObject actionUiParent;
    public GameObject staminaActionParent;
    public GameObject fuelActionParent;
    //private GameObject actionButton;
    private TMP_Text nameText;
    public GameObject actionButtonPrefab;
    public PlayerInformation playerInformation;
    ActionListScriptableObject currentlyAvailablePlayerActions;
    public ActionsWaitingToExecuteList actionsWaitingToExecute;
    public FloatVariableWithMax playerStamina;
    public FloatVariableWithMax playerFuel;
    public FloatVariable actionTrackIndex;

    public ActionTrackManager actionTrackManager;

    public Color fuelActionBgColor;
    public Color staminaActionBgColor;

    public UnityEvent refreshUi;

    private GameObject currentlySelectedButton;
    private GameObject previouslySelectedButton;

    UnityEvent actionButtonClicked;
    // Start is called before the first frame update
    void Start()
    {
        UpdatePlayerUi();
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void UpdatePlayerUi(){
        foreach(Transform child in fuelActionParent.transform){
            Destroy(child.gameObject);
        }
        foreach(Transform child in staminaActionParent.transform){
            Destroy(child.gameObject);
        }
        foreach(ActionListScriptableObject list in playerInformation.availableMovesets.list){
            foreach(ActionInformationHolder action in list.list){
                GameObject actionButton;
                
                if(action.usesFuel){
                    actionButton = Instantiate(actionButtonPrefab, fuelActionParent.transform);
                }
                else{
                    actionButton = Instantiate(actionButtonPrefab, staminaActionParent.transform);
                }
                ActionButtonElements actionButtonElements = actionButton.GetComponent<ActionButtonElements>();
                action.actionButton = actionButton;
                actionButtonElements.nameText.text = action.actionName;
                actionButtonElements.descriptionText.text = action.actionDescription;
                actionButtonElements.icon.sprite = action.actionIconSprite;
                actionButtonElements.icon.color = action.actionIcon.GetComponent<Image>().color;
                actionButtonElements.damageInfo.text =action.baseValue.ToString();
                actionButtonElements.staminaInfo.text =(action.baseStaminaCost + action.staminaModifier).ToString();
                actionButtonElements.action = action;
                actionButtonElements.mainButton.onClick.AddListener(delegate{OnActionButtonClick(actionButton);});
                //actionButtonElements.mainButton.OnPointerEnter.AddListener(delegate{TooltipManager.ShowTooltip_Static(action.actionDescription);})
                //actionButton.transform.Find("Stamina/DecreaseStamina").GetComponent<Button>().onClick.AddListener(delegate{AdjustStaminaCost(action, -1);});
                // actionButton.transform.Find("Stamina/IncreaseStamina").GetComponent<Button>().onClick.AddListener(delegate{AdjustStaminaCost(action, +1);});
                //actionButton.transform.Find("Stamina/DecreaseStamina").GetComponent<Button>().onClick.AddListener(delegate{ChooseSide(action);});
                //actionButton.transform.Find("Stamina/IncreaseStamina").GetComponent<Button>().onClick.AddListener(delegate{ChooseSide(action);});
                if (action.usesFuel){
                    //actionButton.transform.Find("ActionBg").GetComponent<Image>().color = fuelActionBgColor;
                }
                else{
                   // actionButton.transform.Find("ActionBg").GetComponent<Image>().color = staminaActionBgColor;
                }
                if(action.isTargetingLeft){
                    //action.actionButton.transform.Find("Stamina/TargetIndicator").GetComponent<TMP_Text>().text = "Left";
                }
                else{
                    //action.actionButton.transform.Find("Stamina/TargetIndicator").GetComponent<TMP_Text>().text = "Right";
                }
            }
        }
        refreshUi.Invoke();
    } 
    public void OnActionButtonClick(GameObject actionButton){
        if(currentlySelectedButton != null){
            currentlySelectedButton.GetComponent<ActionButtonElements>().bg.color = Color.white;

        }

        if(actionButton != currentlySelectedButton){
            currentlySelectedButton = actionButton;
            currentlySelectedButton.GetComponent<ActionButtonElements>().bg.color = Color.black;
        }
        else{
            currentlySelectedButton = null;
        }
    }




    public void OnActionTrackClick(){
        Debug.Log("Action Track was clicked!");
        if(currentlySelectedButton != null){
            ActionInformationHolder action = currentlySelectedButton.GetComponent<ActionButtonElements>().action;
            if(action.usesFuel){
            if(action.baseStaminaCost <= playerFuel.value){
                if((action.actionType == "Buff" || action.actionType == "Debuff") && actionTrackIndex.value == 0){
                    actionsWaitingToExecute.playerHead.Add(action);
                    playerFuel.value -= action.baseStaminaCost;
                }
                else if (actionTrackIndex.value == 1){
                    actionsWaitingToExecute.playerLeft.Add(action);
                    playerFuel.value -= action.baseStaminaCost;
                }
                else if (actionTrackIndex.value == 2){
                    actionsWaitingToExecute.playerRight.Add(action);
                    playerFuel.value -= action.baseStaminaCost;
                }

                
                actionTrackManager.UpdateActionTrack();
            }
            refreshUi.Invoke();  
            }
            else{
                if(action.baseStaminaCost <= playerStamina.value){
                    if((action.actionType == "Buff" || action.actionType == "Debuff")&& actionTrackIndex.value == 0){
                        actionsWaitingToExecute.playerHead.Add(action);
                        playerStamina.value -= action.baseStaminaCost;
                    }
                    else if (actionTrackIndex.value == 1){
                        actionsWaitingToExecute.playerLeft.Add(action);
                        playerStamina.value -= action.baseStaminaCost;
                    }
                    else if (actionTrackIndex.value == 2){
                        actionsWaitingToExecute.playerRight.Add(action);
                        playerStamina.value -= action.baseStaminaCost;
                    }

                    
                    actionTrackManager.UpdateActionTrack();
                }
                refreshUi.Invoke();
            }
            currentlySelectedButton.GetComponent<ActionButtonElements>().bg.color = Color.white;
            currentlySelectedButton = null;
        }  
    }
    // public void AdjustStaminaCost(ActionInformationHolder action, int value){
    //     if(action.staminaModifier + value >= -2 && action.staminaModifier <= 0){
    //         Debug.Log("modified Stamina");
    //         action.staminaModifier = Mathf.Clamp((action.staminaModifier + value), -2, 0);
    //         action.actionButton.transform.Find("Stamina/StaminaInfo").GetComponent<TMP_Text>().text ="Stamina Cost: " + (action.baseStaminaCost + action.staminaModifier).ToString();
    //     }
    // }

    public void ChooseSide(ActionInformationHolder action){
        Debug.Log("Switched target side");
        action.isTargetingLeft = !action.isTargetingLeft;
        if(action.isTargetingLeft){
            action.actionButton.transform.Find("Stamina/TargetIndicator").GetComponent<TMP_Text>().text = "Left";
        }
        else{
            action.actionButton.transform.Find("Stamina/TargetIndicator").GetComponent<TMP_Text>().text = "Right";
        }
    }

    void OnDestroy(){
        foreach(ActionInformationHolder action in playerInformation.currentMoveset.list){
            action.staminaModifier = 0;
        }
    }
}
