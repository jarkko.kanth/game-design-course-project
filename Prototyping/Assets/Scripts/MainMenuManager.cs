using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Video;

public class MainMenuManager : MonoBehaviour
{
    public PlayerInformation playerInformation;
    public FloatVariable enemiesDefeated;
    public FloatVariable encountersCleared;
    public GameObject introAnimation;
    public GameObject bgAnimation;
    public GameObject menuParent;
    public bool skipAnimation = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void OnStartClick(){
        ResetPlayerStatsForNewGame();
        introAnimation.SetActive(true);
        bgAnimation.SetActive(false);
        menuParent.SetActive(false);
        if(skipAnimation){
            SceneManager.LoadScene(1);
        }
        else{
            StartCoroutine(CheckAnimation());

        }
        
        //
    }
    public void OnQuitClick(){
        Application.Quit();
    }

    public void ResetPlayerStatsForNewGame(){
        playerInformation.hp.max = 100;
        playerInformation.hp.value = playerInformation.hp.max;
        playerInformation.fuel.value = playerInformation.fuel.max = 5;
        playerInformation.stamina.value = playerInformation.stamina.max = 5;
        playerInformation.isDead = false;
        enemiesDefeated.value = 0;
        encountersCleared.value = 0;

    }
    IEnumerator CheckAnimation(){
        yield return new WaitForSeconds(1);
        while(introAnimation.GetComponent<VideoPlayer>().isPlaying){
            yield return new WaitForSeconds(1);
        }
        SceneManager.LoadScene(1);

    }

}
