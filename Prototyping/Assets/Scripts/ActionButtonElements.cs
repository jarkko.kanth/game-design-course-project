using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.EventSystems;

public class ActionButtonElements : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public TMP_Text nameText;
    public TMP_Text descriptionText;
    public TMP_Text damageInfo;
    public TMP_Text staminaInfo;
    public TMP_Text targetSide;
    public Image icon;
    public Image bg;
    public Button mainButton;
    public Button leftButton;
    public Button rightButton;
    public ActionInformationHolder action;

    public void OnPointerEnter(PointerEventData eventData){
        Debug.Log("Mouse is over " + nameText.text);
        TooltipManager.ShowTooltip_Static(action.actionDescription);
    }
    public void OnPointerExit(PointerEventData eventData){
        TooltipManager.HideTooltip_Static();
    }


}
