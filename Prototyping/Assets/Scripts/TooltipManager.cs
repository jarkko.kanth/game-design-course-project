using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TooltipManager : MonoBehaviour
{
    private static TooltipManager instance;
    public Camera uiCamera;
    public TMP_Text tooltipText;
    public RectTransform background;
    private void Awake(){
        instance = this;
        this.gameObject.SetActive(false);

    }
    void Update(){
        Vector2 localPoint;
        RectTransformUtility.ScreenPointToLocalPointInRectangle(transform.parent.GetComponent<RectTransform>(), Input.mousePosition, uiCamera, out localPoint);
        transform.localPosition = localPoint;
        //ShowTooltip("tooltio");

    }
    private void ShowTooltip(string message){
        gameObject.SetActive(true);
        tooltipText.text = message;
        float textPaddingSize = 4f;
        Vector2 backgroundSize = new Vector2(tooltipText.preferredWidth + textPaddingSize * 2, tooltipText.preferredHeight + textPaddingSize * 2);
        background.sizeDelta = backgroundSize;
    }
    private void HideTooltip(){
        gameObject.SetActive(false);
    }
    public static void ShowTooltip_Static(string message){
        Debug.Log(message);
        instance.ShowTooltip(message);
    }
    public static void HideTooltip_Static(){
    instance.HideTooltip();
    }


}
