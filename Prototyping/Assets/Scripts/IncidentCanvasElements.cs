using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class IncidentCanvasElements : MonoBehaviour
{
    public TMP_Text descriptionText;
    public TMP_Text consequenceText;
    public List<Button> choiceButtonList = new List<Button>();
    public Button proceedButton;
    public Image incidentImage;
    public Image incidentBackground;
    public FloatVariable encountersCleared;

    public void OnChoiceButtonClick(){
        foreach(Button button in choiceButtonList){
            button.gameObject.SetActive(false);
        }
        descriptionText.gameObject.SetActive(false);
        consequenceText.gameObject.SetActive(true);
        proceedButton.gameObject.SetActive(true);
    }
    public void OnProceedButtonClick(){
        this.gameObject.SetActive(false);
        encountersCleared.value++;
        //SceneManager.LoadScene(2);
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
